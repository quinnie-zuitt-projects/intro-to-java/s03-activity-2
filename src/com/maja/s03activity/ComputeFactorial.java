package com.maja.s03activity;

import java.util.Scanner;
public class ComputeFactorial {
    public static void main(String[] args){


        int ans = 1;
        int i = 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a number:");

        try {
            int num = in.nextInt();

            if (num > 0) {

                while (i <= num) {
                    ans = ans * i;
                    i++;
                }
                System.out.println("The factorial of: " + num + " is " + ans);
            } else System.out.println("Invalid input");
        }
        catch (Exception err) {
            System.out.println("Invalid input; Please input a number");
        }




    }
}
